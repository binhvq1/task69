package com.devcamp.task6940.quanhedang1n;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Quanhedang1nApplication {

	public static void main(String[] args) {
		SpringApplication.run(Quanhedang1nApplication.class, args);
	}

}
